var mongoose = require('mongoose');
var todoDB = mongoose.createConnection('mongodb://localhost:27017/testjob');

var Schema = mongoose.Schema;
var todoSchema = new Schema({
    postedBy:{ type: Schema.Types.ObjectId, ref: "user" },
    name:String,
    description:String,
    duedate:{ type: Date},
    sequence_number:Number
	},{
		collection: 'todo'
	});

module.exports.todo = todoDB.model('todo', todoSchema);




