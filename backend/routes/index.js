var express = require('express');
var UserAPI = require('./api.js').APP;
var API = express.Router();

API.post('/login', function(req,res) {
 UserAPI.login(req.body, function(err, result) {
  if (err) {
     console.log(err);
   } else {
     res.send(result);
  }
 });
});

API.post('/addtodo', function(req, res) {
 UserAPI.todoadd(req.body, function(err, result) {
   if (err) {
     console.log(err);
   } else {
     res.send(result);
   }
 });
});

API.get('/gettodos/:user_id', function(req, res) {
 UserAPI.gettodoslist(req.params, function(err, result) {
   if (err) {
     console.log(err);
   } else {
     res.send(result);
    }
 });
});


API.put('/updatetodo', function(req, res) {
 UserAPI.updatetodoslist(req.body, function(err, result) {
   if (err) {
     console.log(err);
   } else {
     res.send(result);
   }
 });
});


API.put('/updatetodosequence', function(req, res) {
 var data = req.body;
 var count = 0;
 for(var i=0; i < data.length; i++) {
   UserAPI.updatesequence(data[i], function(err, result) {
     if (err) {
       console.log(err);
     } else {
       count++;
       if(count == data.length ) {
         res.send(result);
       }
     }
   });
 }  
});

API.delete('/removelist/:id', function(req, res) {
 UserAPI.deletetodo(req.params, function(err, result) {
   if (err) {
     console.log(err);
   } else {
     res.send(result);
   }
 });
});


exports = module.exports = API;