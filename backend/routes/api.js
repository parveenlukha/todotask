var db = require('../db/userDB.js');
var db1 = require('../db/todoDB.js');

module.exports.APP = {
 login: function(user, callback) {
   db.user.findOne({
     'username': user.username,
     'password': user.password
   }, function(err, res) {
     if (err) {
       callback(err, null);
     } else {
         if (!res) {
           res = { "error": "usename or password does't match" }
         }
         callback(null, res);
       }
   });
 },
 todoadd: function(todo, callback) {
   db1.todo.create({
       'postedBy': todo.postedBy,
       'name': todo.name,
       'description': todo.description,
       'duedate': todo.duedate,
       'sequence_number': 1
   }, function(err, res) {
     if (err) {
       callback(err, null);
     } else {
       callback(null, res);
     }
   });
 },

 gettodoslist: function(id, callback) {
   db1.todo.find({ "postedBy": id.user_id }).sort({sequence_number:1}).exec(function(err, res) {
     if (err) {
       console.log(err);
     } else {
       callback(null, res);
     }
   });
 },

 updatetodoslist : function(data, callback) {
   db1.todo.findByIdAndUpdate({"_id": data._id},{$set:{"name":data.name,"description":data.description}}
      ,{multi:true},function(err, res) {
         if (err) {
           console.log(err);
         } else {
           callback(null, res);
         }
   });
 },
 updatesequence : function(data, callback) {
   db1.todo.findByIdAndUpdate({"_id": data._id},{$set:{"sequence_number":data.sequence_number}}
      ,{multi:true},function(err, res) {
         if (err) {
           console.log(err);
         } else {
           callback(null, res);
         }
   });
 },
   
deletetodo: function(id, callback) {
db1.todo.remove({ "_id": id.id },
   function(err, res) {
     if (err) {
       console.log(err);
     } else {
       callback(null, res);
     }
   });
 }
}