var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = express();
var mongoose = require('mongoose')

var whiteList = {
   "http://localhost:4000": true,
   "http://localhost:3000": true
 };

var allowCrossDomain = function(req, res, next) {  
       if(whiteList[req.headers.origin]){            
           res.header('Access-Control-Allow-Credentials', true);
           res.header('Access-Control-Allow-Origin', req.headers.origin);
           res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
           res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, Origin, Accept');        
           next();
       } 
};
app.use(allowCrossDomain);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

var routes = require('./routes/index');
app.use('/', routes);

app.use('*',function(req, res){
res.send("Page not found 404");
});


app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.post('/login', function(req, res) {
    res.send('Hello from route handler login');
});


if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send('error', {
      message: err.message,
      error: err
    });
  });
}


app.listen(4000,function(){
  console.log("server running at port 4000");
});

