# TestJob

### Version
0.0.1
### Tech

* [AngularJS] - HTML enhanced for web apps!
* [Bootstrap] - great UI boilerplate for modern web apps
* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework

### Installation

Testjob requires [Node.js](https://nodejs.org/) v0.11.14 to run.

### Frontend setup
```sh
$ git clone [https://parveenlukha@bitbucket.org/parveenlukha/todotask.git]
$ cd todotask
$ cd frontend
$ npm install
$ cd public
$ bower install
$ cd ..
$ node --harmony server.js 
```

### Backend Setup:

```sh
$ cd todotask
$ cd backend
$ npm install
$ node server.js
```

###### Note: Create one user directly in mongo :

```sh
$ mongo
$ use testjob
$ db.user.insert({"username":"getStatus","password":"test123"})
```