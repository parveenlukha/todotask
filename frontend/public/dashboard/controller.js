app.controller('dashboardCtrl',["$http","$scope","$state","$cookies","dasboardData",function($http,$scope,$state,$cookies,dasboardData) {
   if(!$cookies.userdata) {
        $state.go("login");
    } 
 $scope.user = JSON.parse($cookies.userdata);
 dasboardData.fetchList($scope.user.id).then(function(data) {
   $scope.todoListData = angular.copy(data);
       $scope.models = {
       selected: null,
       lists: {"A": []}
   };
   $scope.models.lists.A = $scope.todoListData; 
 })

    $scope.todoDataChange = function(value,id) {
   if(((value.which === 13) || (value === "1"))) {
     for(var i=0; i < $scope.todoListData.length; i++) {
       if(id === $scope.todoListData[i]._id) {
           var updatedata = {
               name: $scope.todoListData[i].name,
               description: $scope.todoListData[i].description,
               id:id
         }
       }
     }
     dasboardData.updateTodo(updatedata)
     .then(function(data){}) 
   }
 }

 $scope.newTodo = function() {
     $state.go('Newtodo');
 }

 $scope.updateSequence = function() {
   for (var i = 0; i < $scope.todoListData.length; i++) {
       $scope.todoListData[i].sequence_number = i+1;
   }
   dasboardData.updateTodoList($scope.todoListData)
    .then(function(data) {
   })
 }

 $scope.deleteItem = function(id,index) {
   dasboardData.deleteList(id).then(function(data) {
     $scope.models.lists.A.splice(index,1);
   })
 }

}]);