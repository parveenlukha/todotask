app.factory("list", [ "angularconfig", "$resource", function(angularconfig, $resource) {
  return $resource(angularconfig.url + ':'+ angularconfig.port + '/gettodos/:user_id', {
   user_id : '@user_id'
  }, {
      query: {
          method: "GET",
          isArray:true
      },
      delete: {
          method: "DELETE"
      }
  });
}]);


app.factory("updatetodos", [ "angularconfig", "$resource", function(angularconfig, $resource) {
  return $resource(angularconfig.url + ':'+ angularconfig.port + '/updatetodo', {
  }, {
      update: {
          method: "PUT"
      },
      delete: {
          method: "DELETE"
      }
  });
}]);

app.factory("updateTodosList", [ "angularconfig", "$resource", function(angularconfig, $resource) {
  return $resource(angularconfig.url + ':'+ angularconfig.port + '/updatetodosequence', {
  }, {
      update: {
          method: "PUT"
      },
      delete: {
          method: "DELETE"
      }
  });
}]);

app.factory("removelist", ["angularconfig", "$resource", function(angularconfig, $resource) {
 return $resource(angularconfig.url + ':'+ angularconfig.port + '/removelist/:id',{ id: "@id"}, 
    {
     update: {
         method: "PUT"
     },
     delete: {
         method: "DELETE"
     }
 });
}]);

app.factory("dasboardData", ["$http", "$q","list","updatetodos","updateTodosList","removelist",function($http,$q,list,updatetodos,updateTodosList,removelist) {
  var userData = {};
  var selected;
  return {
     fetchList: function(user_id) {
          var defer = $q.defer();
          try {
              list
                  .query({
                      user_id: user_id
                  }, function(resp) {
                      defer.resolve(resp);
                  }, function(err) {
                      defer.reject([]);
                      console.log(err);
                  });
          } catch (e) {
              console.log(e.stack);
              defer.reject([]);
          }
           return defer.promise;
     },
     updateTodo: function(data) {
          var defer = $q.defer();
          try {
              updatetodos
                  .update({
                      _id: data.id,
                      name:data.name,
                      description:data.description
                  }, function(resp) {
                      defer.resolve(resp);
                  }, function(err) {
                      defer.reject([]);
                      console.log(err);
                  });
          } catch (e) {
              console.log(e.stack);
              defer.reject([]);
          }
          return defer.promise;
     },
     updateTodoList: function(data) {
          var defer = $q.defer();
          try {
              updateTodosList
                  .update(data, function(resp) {
                      defer.resolve(resp);
                  }, function(err) {
                      defer.reject([]);
                      console.log(err);
                  });
          } catch (e) {
              console.log(e.stack);
              defer.reject([]);
          }
          return defer.promise;
     },
     deleteList: function(category_code) {
       var defer = $q.defer();
         try {
             removelist
                 .delete({
                  "id":category_code
                 }, function(resp) {
                     cartlist = resp;
                     defer.resolve(cartlist);
                 }, function(err) {
                     cartlist = [];
                     defer.reject(err);
                     console.log(err);
                 });
         } catch (e) {
             console.log(e.stack);
             cartlist = [];
             defer.reject([]);
         }
         return defer.promise;         
     }
}
}]);