app.controller('newtodoCtrl',["$http","$scope","$cookies","$state","todoData",function($http,$scope,$cookies,$state,todoData) {
   if(!$cookies.userdata) {
        $state.go("login");
    } 
     $scope.todo = {};
 $scope.user = JSON.parse($cookies.userdata);

 $scope.addtodo = function() {
     if($scope.todo.name && $scope.todo.description && $scope.todo.duedate) {
       $scope.todo.postedBy = $scope.user.id; 
       todoData.saveTodoData($scope.todo).then(function(data) {
         $state.go('dashboard');
       })
     } else {
         $scope.errorMessage = "Please enter details";
   }
 }
 $scope.home = function() {
   $state.go('dashboard');
 }
}])