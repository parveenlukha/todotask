app.factory("todo", [ "angularconfig", "$resource", function(angularconfig, $resource) {
  return $resource(angularconfig.url + ':'+ angularconfig.port + '/addtodo', {
  },{
    save: {method:'POST', withCredentials:true}
  });
}]);


app.factory("todoData", ["$http", "$q","todo", function($http,$q,todo) {
  var userData = {};
  var selected;
  return {
       saveTodoData: function(user) {
          var defer = $q.defer();
          try {
              todo
                  .save({
                      name: user.name,
                      description: user.description,
                      duedate:user.duedate,
                      postedBy :user.postedBy
                  }, function(resp) {
                      userData = resp;
                      defer.resolve(userData);
                  }, function(err) {
                      userData = {};
                      defer.reject(err);
                  });
          } catch (e) {
              userData = {};
              defer.reject({});
          }
          return defer.promise;
      }  
  }
}]);