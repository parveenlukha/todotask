'use strict';
app.factory('angularconfig', function(){
    // BE side
    var url = 'http://localhost';
    var host = 'http://localhost';
    var port = 4000;
    var FEurl = 'http://localhost';
    var FEhost = 'http://localhost';
    var FEport = 3000;
    return {
      getUrl : function() {
        return url;
      },
      url:url,
      port:port,
      FEport:FEport,
    }
});
