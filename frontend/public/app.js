var app = angular.module('mani', ['ui.router','ngResource','ngMessages','ngCookies','dndLists']);

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $stateProvider.state('login', {
       url: '/login',
       templateUrl: 'login/login.html',       
       controller: 'loginCtrl'
    }).state('dashboard', {
       url: '/dashboard',
       templateUrl: 'dashboard/dashboard.html',       
       controller: 'dashboardCtrl'
    }).state('Newtodo', {
       url: '/newtodo',
       templateUrl: 'newtodo/newtodo.html',       
       controller: 'newtodoCtrl'
    })
    $httpProvider.defaults.withCredentials = true;
    $urlRouterProvider.otherwise('/login');
});
