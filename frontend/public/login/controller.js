app.controller('loginCtrl',["$http","$scope","$state","$cookies","$rootScope","loginData","$cookieStore",function($http,$scope,$state,$cookies,$rootScope,loginData,$cookieStore) {
    if($cookies.userdata) {
        $state.go("dashboard");
    }
    $scope.loginDetails = {};
    $scope.submitLoginDetails = function() {
        if($scope.loginDetails.username && $scope.loginDetails.password) {
            loginData.fetchData($scope.loginDetails).then(function(data){
          if(data.username) {
              $cookieStore.put('userdata',{"id":data._id,"username":data.username});
              $rootScope.$emit("userData",{"id":data._id,"username":data.username});
              $state.go("dashboard");
          }
          if(data.error) {
              $scope.errorMessage = "Username or Password does not match";
          }
        })
        }
    }
}]);