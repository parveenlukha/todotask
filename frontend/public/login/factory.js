app.factory("login", [ "angularconfig", "$resource", function(angularconfig, $resource) {
 return $resource(angularconfig.url + ':'+ angularconfig.port + '/login', {
 },{
    save: {method:'POST', withCredentials:true}
 });
}]);


app.factory("loginData", ["$http", "$q","login", function($http,$q,login) {
  var userData = {};
  var selected;
  return {
       fetchData: function(user) {
          var defer = $q.defer();
          try {
            login
                .save({
                    username: user.username,
                    password: user.password
                }, function(resp) {
                    userData = resp;
                    defer.resolve(userData);
                }, function(err) {
                    userData = {};
                    defer.reject(err);
                });
          } catch (e) {
              userData = {};
              defer.reject({});
          }
          return defer.promise;
      }  
  }
}]);