var express = require('express');
var app = express();

app.use('/',express.static(__dirname + '/public'));

app.use('*',function(req, res){
res.send("Page not found 404");
});

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4000');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.listen(3000,function(){
	console.log("server running at port 3000");
});